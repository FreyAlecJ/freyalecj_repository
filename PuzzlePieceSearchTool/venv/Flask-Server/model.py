from sqlalchemy import create_engine, MetaData, Table
from . import db

class PieceImages(db.Model):
        __tablename__ = 'pieceimages'
        image_id = db.Column(db.Integer(), primary_key=True)
        image_name = db.Column(db.String(100), nullable=False)
        image_path = db.Column(db.String(255), nullable=False)
        section = db.Column(db.Integer(), primary_key=True)
        matched = db.Column(db.Boolean(), nullable=False)

        def __repr__(self):
            return "<{}:{}>".format(self.section, self.image_id)

