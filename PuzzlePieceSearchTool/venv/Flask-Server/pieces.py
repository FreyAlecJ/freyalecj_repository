from flask import Blueprint, jsonify, request
from . import db
from .model import PieceImages

piecesBP = Blueprint("piecesBP", __name__)

@piecesBP.route("/matchpiece", methods=["PATCH"])
def matchPiece():

    piece_data = request.get_json()

    puzzlePiece = PieceImages.query.filter(PieceImages.image_id == piece_data["id"], PieceImages.section == piece_data["section"]).first()

    puzzlePiece.matched = True

    db.session.add(puzzlePiece)

    db.session.commit()

    return "Done", 201


@piecesBP.route("/getpieces", methods=["GET"])
def fetchPieces():
    
    piece_list = PieceImages.query.filter(PieceImages.matched == False).order_by(PieceImages.section, PieceImages.image_id).all()
    
    pieces = []

    for piece in piece_list:
        path = piece.image_path + '/' + piece.image_name
        pieces.append({"id" : piece.image_id, "section": piece.section, "path" : path, "matched" : piece.matched})

    response = jsonify({"Pieces" : pieces})

    return response