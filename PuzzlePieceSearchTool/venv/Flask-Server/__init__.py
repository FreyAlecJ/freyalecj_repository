from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

db = SQLAlchemy()

def create_app():
    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:PASSWORD@localhost:3306/puzzlepiecesearchtool'
    CORS(app)

    db.init_app(app)
    
    from .pieces import piecesBP
    app.register_blueprint(piecesBP)

    return app

