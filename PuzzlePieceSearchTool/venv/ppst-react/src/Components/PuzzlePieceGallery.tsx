import React from 'react'
import * as ReactRedux from 'react-redux';
import { bindActionCreators} from 'redux'
import { AgGridReact } from 'ag-grid-react';
import { CellValueChangedEvent, ColDef } from 'ag-grid-community';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css'
import { PuzzlePiece } from '../Types/PuzzlePiece';
import { AppState } from '../Types/AppState';
import { ThunkDispatch} from 'redux-thunk';
import { AppActions } from '../Types/Actions';
import { startMatchPiece, startUpdatePieceId, startUpdatePieces } from '../Actions/SearchToolActions';

interface GalleryProps {
}

interface GalleryState{
}

type Props = GalleryProps & StateProps & StateDispatchProps;

class PuzzlePieceGallery extends React.Component<Props, GalleryState >{ 
    
    columnDefs : ColDef[]  =  [
            {headerName: "Piece Id", field: 'id', sortable: true, filter: true, editable:true,},
            {headerName: "Piece Section", field: 'section', sortable: true, filter: true},
            {headerName: "Piece Path", field: 'path', sortable: true, filter: true},
            {headerName: "Is Matched", field: 'isMatched', sortable: true, filter: true, checkboxSelection: true},
    ]


    onButtonClick = () => {
        //should be a single row data
        const selectedRow  = this.gridApi.getSelectedRows()[0];
        this.props.startMatchPiece(selectedRow.id, selectedRow.section);
    }

    onCellValueClick = (event : CellValueChangedEvent) => {
        
        switch (event.colDef.field){
            case "id":
                this.props.startUpdatePieceId(this.props.pieces[event.rowIndex], event.newValue);
                break;
            case "section":
                break;
            case "path":
                break;
            case "isMatched":
                break;
            default: 
                console.error("Unknown column editted within the grid: " + event.colDef.field?.toString());
        }
    }

    componentDidMount = () => {
        this.props.startUpdatePieces()
    }
    

    gridApi;

    render() {
        return (
            <div className="ag-theme-balham"
                style= {{
                    width: 1000,
                    height: 500
                }}>
                <AgGridReact 
                    columnDefs={this.columnDefs} 
                    rowData={this.props.pieces.map(piece => ({id : piece.id, section: piece.section, path: piece.path, isMatched: piece.isMatched} ) )}
                    rowSelection = "single"
                    onGridReady={params => this.gridApi = params.api}
                    onCellValueChanged= {this.onCellValueClick} />
                <button onClick={this.onButtonClick}>Match Piece!</button>
            </div>
        )
    }
}

interface StateProps {
    pieces : PuzzlePiece[]
}

interface StateDispatchProps { 
    startMatchPiece : (pieceId : number, pieceSection : number) => void;
    startUpdatePieceId: (piece : PuzzlePiece, newId : number) => void;
    startUpdatePieces : () => void;
}

const mapStateToProps = (state : AppState , ownprops : GalleryProps): StateProps => ({
    pieces : state.SearchToolState.pieces
});

const mapDispatchToProps = (dispatch : ThunkDispatch< any, any, AppActions> , ownProps : GalleryProps): StateDispatchProps => ({
    startMatchPiece : bindActionCreators(startMatchPiece, dispatch),
    startUpdatePieceId : bindActionCreators(startUpdatePieceId, dispatch),
    startUpdatePieces : bindActionCreators(startUpdatePieces, dispatch)
});

export default ReactRedux.connect(mapStateToProps, mapDispatchToProps, null, {forwardRef: true }) (PuzzlePieceGallery);

