import PuzzlePieceGallery from '../PuzzlePieceGallery';
import renderer from 'react-test-renderer';
import React from 'react';
import delay from 'redux-saga';
import { AppState } from '../../Types/AppState';
import * as Redux from 'redux';
import rootReducer from '../../Reducers/reducerIndex';

describe('The Gallery component.', () => {

    let container;
    let store : AppState;

    //probably should just mock this stuff versus using the legit deals
    beforeEach( () => {
        store = { 
            SearchToolState: {
                    currentIndex : 0,
                    piecesRemaining : 6,
                    pieces : [ {id : 1, section: 1, path : "Alecs_Path", isMatched : false, puzzleId: 1}]
            }
        };

        container = Redux.createStore(rootReducer, store);

    });


    describe('The state props', () => {

    });
    describe('The view does not regress', async () => {
            const tree = renderer.create(
                <PuzzlePieceGallery/>
            )

            await delay();

            const instance = tree.root;
            
    });
});