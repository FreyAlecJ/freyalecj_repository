import PuzzlePieceComponent, {mapStateToProps, mapDispatchToProps } from "../PuzzlePieceComponent";
import renderer from 'react-test-renderer';
import React from 'react';
import rootReducer, { AppState } from '../../Reducers/reducerIndex';
import * as ReactRedux from 'react-redux';
import * as Redux from 'redux';


describe('The Puzzle Piece Component', () => {

    let container;
    let store : AppState;

    //probably should just mock this stuff versus using the legit deals
    beforeEach( () => {
        store = { 
            SearchToolState: {
                    currentIndex : 0,
                    piecesRemaining : 6,
                    pieces : [ {id : 1, section: 1, path : "Alecs_Path", isMatched : false, puzzleId: 1}]
            }
        };

        container = Redux.createStore(rootReducer, store);

    });

    describe('The Container Element', () => {
        describe ('mapStateToProps', () => {
            it ("should map the state to props correctly", () => {
                    
                //call the mapstate to props with the state and get the return types back 
                    const state = mapStateToProps(store);

                    //assert that those things were correct
                    expect(state.id).toEqual(1);
                    expect(state.section).toEqual(1);
                    expect(state.path).toEqual("Alecs_Path");
                    expect(state.isMatched).toEqual(false);
            });
        });
    });

    describe('The Display element', () => {
        it ("should not regress", () => {
            const tree = renderer.create (
                <ReactRedux.Provider store={container}>
                    <PuzzlePieceComponent/>
                </ReactRedux.Provider>
            );

            expect(tree.toJSON()).toMatchSnapshot();
        });
    });
});