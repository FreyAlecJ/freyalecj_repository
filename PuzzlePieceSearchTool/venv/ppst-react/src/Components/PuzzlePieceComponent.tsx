import React, { CSSProperties } from 'react';
import * as ReactRedux from 'react-redux';
import { ThunkDispatch} from 'redux-thunk';
import { AppActions } from '../Types/Actions';
import { bindActionCreators } from 'redux';
import * as searchToolActions from '../Actions/SearchToolActions';
import { AppState } from "../Reducers/reducerIndex"

type Props =  StateProps & DispatchProps;

const PuzzlePieceComponent = ReactRedux.connect(mapStateToProps, mapDispatchToProps) (
    function ({id, section, path, isMatched, startMatchPiece} : Props)  {

        let onClickHandler = (event) => {
            startMatchPiece(id, section);
        }

        return ( 
            <div>
                <img src={path} style={puzzlePieceImageStyle.top} alt="images/scuba.jpg" />
                <img src={path} style={puzzlePieceImageStyle.right} alt="images/scuba.jpg"/>
                <img src={path} style={puzzlePieceImageStyle.bottom} alt="images/scuba.jpg"/>
                <img src={path} style={puzzlePieceImageStyle.left} alt="images/scuba.jpg"/>
                <p>ID:{id}          Section: {section}      Path: {path}        IsMatched: {String(isMatched)} </p>
                <button onClick={onClickHandler} >Match Piece!</button>
            </div>
        )
    }
);

interface StateProps{
    id :number,
    section : number,
    path : string,
    isMatched : boolean
}

interface DispatchProps {
    startMatchPiece : (id :number, section: number ) => void
}

export function mapStateToProps(state: AppState) : StateProps {
    return {
        id : state.SearchToolState.pieces[state.SearchToolState.currentIndex].id,
        section : state.SearchToolState.pieces[state.SearchToolState.currentIndex].section,
        path : state.SearchToolState.pieces[state.SearchToolState.currentIndex].path,
        isMatched : state.SearchToolState.pieces[state.SearchToolState.currentIndex].isMatched
    };
}

export function mapDispatchToProps(dispatch : ThunkDispatch <any, any, AppActions>) : DispatchProps {
    return {
        startMatchPiece : bindActionCreators(searchToolActions.startMatchPiece, dispatch)
    }
}

export interface StyleDictionary {
    [Key: string] : CSSProperties
}

const puzzlePieceImageStyle : StyleDictionary = {
    top : {
        margin : "50px",
        width : "250px",
        height : "150px",
        transform: "rotate(0deg)"
    },
    right : {
        margin : "50px",
        width : "250px",
        height : "150px",
        transform: "rotate(180deg)"
    },
    left : {
        margin : "50px",
        width : "250px",
        height : "150px",
        transform: "rotate(90deg)"
    },
    bottom : {
        margin : "50px",
        width : "250px",
        height : "150px",
        transform: "rotate(270deg)"
    }
}

 
export default PuzzlePieceComponent;
