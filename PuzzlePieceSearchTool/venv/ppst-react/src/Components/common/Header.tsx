import React from 'react';
import { NavLink} from 'react-router-dom';

const Header = () => {
    return (
         <nav> 
             <NavLink to="/" exact> Search Page </NavLink> { " | "}
             <NavLink to="/Gallery" > Gallery </NavLink> { " | " }
             <NavLink to="/AddPuzzlePiece" > Add Piece </NavLink>
        </nav>
    );
}

export default Header;