import React, {useEffect} from 'react';
import '../App.css';
import PuzzlePieceComponent from './PuzzlePieceComponent';
import * as ReactRedux from 'react-redux'
import { ThunkDispatch } from 'react-thunk';
import { AppActions } from '../Types/Actions';
import { bindActionCreators} from 'redux'
import { startGetPrevPiece, startGetNextPiece, startUpdatePieces } from '../Actions/SearchToolActions';
import { AppState } from '../Reducers/reducerIndex';

type Props = StateProps & DispatchProps;

const PuzzlePieceSearchTool = ReactRedux.connect(mapStateToProps, mapDispatchToProps) (
  function ({currentIndex, piecesRemaining, startGetPreviousAction, startGetNextAction, startUpdatePieces} : Props) {
      
      useEffect( () => {
        startUpdatePieces();
      });

      return (
      <div> 
        <PuzzlePieceComponent/>
        <button onClick={startGetPreviousAction}>Previous Image</button>
        <p> {currentIndex + 1} / {piecesRemaining} </p>
        <button onClick={startGetNextAction}>Next Image</button>
      </div>
    );
});


interface StateProps {
  piecesRemaining : number,
  currentIndex : number
}

interface DispatchProps {
  startGetPreviousAction : () => void,
  startGetNextAction : () => void,
  startUpdatePieces : () => void
}

function mapStateToProps(state : AppState) : StateProps {
  return {
    piecesRemaining : state.SearchToolState.piecesRemaining,
    currentIndex : state.SearchToolState.currentIndex
  }
}

function mapDispatchToProps(dispatch : ThunkDispatch <any, any, AppActions>) : DispatchProps {
  return {
    startGetPreviousAction : bindActionCreators(startGetPrevPiece, dispatch),
    startGetNextAction : bindActionCreators(startGetNextPiece, dispatch),
    startUpdatePieces : bindActionCreators(startUpdatePieces, dispatch)
  };
}

export default PuzzlePieceSearchTool;
