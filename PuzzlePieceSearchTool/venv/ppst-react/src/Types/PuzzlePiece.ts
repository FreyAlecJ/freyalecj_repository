export interface PuzzlePiece {
    id : number,
    section : number,
    path : string,
    isMatched: boolean
}