import { PuzzlePiece } from '../Types/PuzzlePiece'


export interface PuzzlePieceSearchToolModel {
    currentIndex : number,
    piecesRemaining : number,
    pieces : PuzzlePiece[],
    stateLoaded : boolean
  }