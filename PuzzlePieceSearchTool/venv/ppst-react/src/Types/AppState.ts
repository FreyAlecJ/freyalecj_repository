import { PuzzlePieceSearchToolModel } from './PuzzlePieceSearchToolModel';


export interface AppState{
    SearchToolState : PuzzlePieceSearchToolModel
}