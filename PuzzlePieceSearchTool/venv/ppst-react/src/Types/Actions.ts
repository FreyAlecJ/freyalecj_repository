import { PuzzlePiece } from "./PuzzlePiece"

export const GET_NEXT_PIECE = "GET_NEXT_PIECE"
export const GET_PREV_PIECE = "GET_PREV_PIECE"
export const MATCH_PIECE = "MATCH_PIECE"
export const MARK_PIECE_MATCHED = "MARK_PIECE_MATCHED"
export const UPDATE_PIECE_ID = "UPDATE_PIECE_ID"
export const UPDATE_PIECES = "UPDATE_PIECES"

export interface GetNextPieceAction {
    type: typeof GET_NEXT_PIECE;
}

export interface GetPrevPieceAction {
    type: typeof GET_PREV_PIECE;
}

export interface MatchPieceAction {
    type: typeof MATCH_PIECE;
    pieceId : number;
    pieceSection : number;
}

export interface UpdatePieceIdAction {
    type: typeof UPDATE_PIECE_ID;
    piece : PuzzlePiece;
    newId : number;
}

export interface UpdatePiecesAction {
    type : typeof UPDATE_PIECES;
    pieces : PuzzlePiece[]
}

export type PieceActionTypes = GetNextPieceAction | GetPrevPieceAction | MatchPieceAction | UpdatePieceIdAction | UpdatePiecesAction;

export type AppActions = PieceActionTypes;