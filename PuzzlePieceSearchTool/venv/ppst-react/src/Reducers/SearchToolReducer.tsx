import {PuzzlePiece} from '../Types/PuzzlePiece';
import {PuzzlePieceSearchToolModel} from '../Types/PuzzlePieceSearchToolModel';
import {PieceActionTypes, GET_NEXT_PIECE, GET_PREV_PIECE, MATCH_PIECE, UPDATE_PIECE_ID, UPDATE_PIECES} from '../Types/Actions';

let defaultState : PuzzlePieceSearchToolModel = {
  pieces : [ {id : -1, section : -1, path : "C:/", isMatched : false }],
  piecesRemaining : 0,
  currentIndex : 0,
  stateLoaded : false
}

const fetchNextImage = (model : PuzzlePieceSearchToolModel) => {

    let nextIndex = model.currentIndex + 1;
  
    if (nextIndex >= model.pieces.length)
      return model;
  
    return Object.assign({}, model, {currentIndex: nextIndex})
}
  
const fetchPrevImage = (model : PuzzlePieceSearchToolModel) => {
    let previousIndex = model.currentIndex - 1;
  
    if (previousIndex === -1)
      return model;
  
    return Object.assign({}, model, {currentIndex : previousIndex})
}
  
const matchImage = (model : PuzzlePieceSearchToolModel, id :number, section : number) => {

    let updatedModel : PuzzlePieceSearchToolModel = { ...model , pieces : model.pieces.map( piece => (piece.id === id && piece.section === section) ? { ...piece, isMatched : true} : piece), stateLoaded : false};
    
    return Object.assign ({}, model, updatedModel);
}

const updateImageId = (model: PuzzlePieceSearchToolModel, piece: PuzzlePiece, newId : number) => {

  let updatedModel : PuzzlePieceSearchToolModel = {...model, pieces : model.pieces.map (newPiece => (newPiece.id === piece.id && newPiece.section === piece.section) ? {...newPiece, id : newId} : newPiece )};

  return Object.assign ({}, model, updatedModel);
}

const updatePieces = (model: PuzzlePieceSearchToolModel, pieces: PuzzlePiece[]) : PuzzlePieceSearchToolModel => {
  let newModel : PuzzlePieceSearchToolModel = { 
    pieces : pieces,
    piecesRemaining : pieces.length,
    currentIndex : 0,
    stateLoaded : true
  }

  return Object.assign ({}, model, newModel)
}


function searchToolReducer (
    state: PuzzlePieceSearchToolModel = defaultState,
    action : PieceActionTypes) : PuzzlePieceSearchToolModel {
      switch (action.type){
        case GET_NEXT_PIECE:
          return fetchNextImage(state);
        case GET_PREV_PIECE:
          return fetchPrevImage(state);
        case MATCH_PIECE:
          return matchImage(state,action.pieceId, action.pieceSection);
        case UPDATE_PIECE_ID:
          return updateImageId(state, action.piece, action.newId);
        case UPDATE_PIECES:
          return updatePieces(state, action.pieces);
        default: return state
      }
  };

export default searchToolReducer;