import {combineReducers} from 'redux'
import searchToolReducer from './SearchToolReducer';

const rootReducer = combineReducers ({
    SearchToolState: searchToolReducer,
});

export type AppState = ReturnType<typeof rootReducer>

export default rootReducer;
