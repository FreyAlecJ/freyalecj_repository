import { AppActions } from '../Types/Actions';
import { Dispatch } from 'redux';
import { AppState} from '../Reducers/reducerIndex';
import { PuzzlePiece } from '../Types/PuzzlePiece';

const getNextPiece = (): AppActions => ({
    type: "GET_NEXT_PIECE"
})

const getPrevPiece = () : AppActions => ({
    type: "GET_PREV_PIECE"
})
 
const matchPiece = (pieceId : number, pieceSection : number): AppActions => ({
    type: "MATCH_PIECE",
    pieceId, 
    pieceSection,
})

const updatePieceId = (piece : PuzzlePiece, newId: number) : AppActions => ({
    type: "UPDATE_PIECE_ID",
    piece,
    newId
})


const updatePieces = (pieces: PuzzlePiece[]) : AppActions => ({
    type: "UPDATE_PIECES",
    pieces
})


export const startGetNextPiece = () => {
    return (dispatch : Dispatch<AppActions>, getState:  () => AppState ) => {
        dispatch(getNextPiece());
    }
}

export const startGetPrevPiece = () => {
    return (dispatch : Dispatch<AppActions>, getState: () => AppState ) => {
        dispatch(getPrevPiece());
    }
}

export const startMatchPiece = (pieceId : number, pieceSection: number) => {
    return async (dispatch : Dispatch<AppActions>, getState: () => AppState ) => {

        const piece = { "id" : pieceId, "section" : pieceSection }
        const response = await fetch("http://127.0.0.1:5000/matchpiece", {
            method : "PATCH",
            headers : {
                "Content-Type" : 'application/json'
            },
            body : JSON.stringify(piece)
        })

        //it was actually updated in the DB. Now update it here and reload the list of them
        if (response.ok)
        {
            dispatch(matchPiece(pieceId, pieceSection));
            startUpdatePieces();
        }
        else 
            console.error('Unable to match piece. ID: ' + pieceId + " Section: " + pieceSection)
        
    }
}

export const startUpdatePieceId = (piece : PuzzlePiece, newId: number ) => {
    return (dispatch : Dispatch<AppActions>, getState: () => AppState) => {
        dispatch(updatePieceId(piece, newId));
    }
}

export const startUpdatePieces = () => {
    return (dispatch : Dispatch<AppActions>, getState: () => AppState) => {

        if (getState().SearchToolState.stateLoaded)
            return;

        fetch("http://127.0.0.1:5000/getpieces").then(responseData => responseData.json()).then(data => {
            let remainingPieces : PuzzlePiece[] = data["Pieces"].map(piece => ({id : piece.id, section : piece.section, path : piece.path, isMatched : piece.matched}));
    
            dispatch(updatePieces(remainingPieces))
            })
    }
}