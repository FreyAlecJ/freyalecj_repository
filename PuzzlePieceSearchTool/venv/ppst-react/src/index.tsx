import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import PuzzlePieceSearchTool from './Components/PuzzlePieceSearchTool';
import * as serviceWorker from './serviceWorker';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import PuzzlePieceGallery from './Components/PuzzlePieceGallery';
import { BrowserRouter, Route, Switch} from 'react-router-dom'
import rootReducer from './Reducers/reducerIndex'
import thunk, { ThunkMiddleware } from 'redux-thunk';
import { AppState } from './Reducers/reducerIndex';
import { AppActions } from './Types/Actions';
import './bootstrap.min.css';
import Header from "./Components/common/Header";
import PageNotFound from './Components/PageNotFound';

//todo after we finish the work and want to get into the Db. pass the inital state to the store
let container = Redux.createStore(rootReducer, Redux.applyMiddleware(thunk as ThunkMiddleware<AppState, AppActions>));

function HomePage(){
  return <PuzzlePieceSearchTool />
}

function GalleryPage(){
  return <PuzzlePieceGallery />
}

ReactDOM.render(
  <React.StrictMode>
    <ReactRedux.Provider store={container}>
      <BrowserRouter>
        <React.Fragment>
          <Header/>
          <Switch>
            <Route exact path='/' component={HomePage}/>
            <Route exact path='/Gallery' component={GalleryPage}/>
            <Route component={PageNotFound} />
          </Switch>
        </React.Fragment>
      </BrowserRouter>
    </ReactRedux.Provider>
  </React.StrictMode>,
  document.getElementById('root')
);


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

