CREATE DATABASE PuzzlePieceSearchTool;

USE PuzzlePieceSearchTool;

CREATE TABLE PieceImages(
   image_id INT NOT NULL,
   image_name VARCHAR(100) NOT NULL,
   image_path VARCHAR(255) NOT NULL,
   section INT NOT NULL,
   
   matched bool NOT NULL,
   
   PRIMARY KEY (image_id, section )
);  